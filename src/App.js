import React from 'react';
import './App.css';
import Employees from './routes/Employees/';

function App() {
  return (
    <Employees />
  );
}

export default App;
