import React from 'react';
import './styles.css';

function EmployeeCard(props) {
  const { employee, clickHandler, isActive } = props;
  return (
    <article className={"employeeCard" + (isActive ? " active" : "")} onClick={() => clickHandler(employee)}>
      <img src={employee.avatar} alt={employee.firstName + " " + employee.lastName} />
      <section className="employeeCardDetails">
        <h3>{employee.firstName} {employee.lastName}</h3>
        <span>{employee.bio}</span>
      </section>
    </article>
  );
}

export default EmployeeCard;
