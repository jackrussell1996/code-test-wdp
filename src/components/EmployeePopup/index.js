import React from 'react';
import './styles.css';

function EmployeePopup(props) {
  const { employee, clickHandler } = props;
  return (
    <div className="modal open" onClick={(e) => (e.target.classList.contains('modal') || e.target.classList.contains('modalClose')) && clickHandler()}>
      <div className="modalContent">
      <span className="modalClose">&times;</span>
        <section>
          <img src={employee.avatar} alt={employee.firstName + " " + employee.lastName} />
          <h4>{employee.jobTitle}</h4>
          <div>{employee.age}</div>
          <div>{employee.dateJoined}</div>
        </section>
        <section>
          <h3>{employee.firstName} {employee.lastName}</h3>
          <div>{employee.bio}</div>
        </section>
      </div>
    </div>
  );
}

export default EmployeePopup;
