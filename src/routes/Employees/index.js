import React from 'react';
import './styles.css';

import EmployeeCard from '../../components/EmployeeCard';
import EmployeePopup from '../../components/EmployeePopup';

class Employees extends React.Component {
  constructor() {
    super();
    this.state = {
      employeesData: null,
      employeePopup: null,
      employeeIdActive: null,
    };
    this.fetchEmployees();
  }

  async fetchEmployees() {
    const response = await fetch('/sample-data.json');
    if (response) {
      this.setState({employeesData: await response.json()});
    }
  }

  renderPopup() {
    if (this.state.employeePopup) {
      return <EmployeePopup employee={this.state.employeePopup} clickHandler={this.popupClickHandler.bind(this)}/>;
    }
  }

  popupClickHandler(employee) {
    this.setState({employeePopup: employee, employeeIdActive: employee && employee.id});
  }

  render() {
    if (this.state.employeesData) {
      const { companyInfo, employees } = this.state.employeesData;
      return (
        <React.Fragment>
          {this.renderPopup()}
          <header className="header">
            <div>
              <h1>{companyInfo.companyName}</h1>
              <span>{companyInfo.companyMotto}</span>
            </div>
            <span>Since {companyInfo.companyEst}</span>
          </header>
          <main>
            <h2 className="employeesHeading">Our Employees</h2>
            <section className="employees">
              {employees.map((employee, i) => <EmployeeCard employee={employee} key={i} clickHandler={this.popupClickHandler.bind(this)} isActive={employee.id === this.state.employeeIdActive}/>)}
            </section>
          </main>
        </React.Fragment>
      );
    }
    else {
      return (
        <h1>Loading</h1>
      );
    }
  }
}

export default Employees;
